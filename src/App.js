import React, { useState } from "react";
import { Container, Form, Button } from 'react-bootstrap';

function App() {
  const [calc, setCalc] = useState('')
  const [input1, setInput1] = useState('')
  const [input2, setInput2] = useState('')
  
  
  function ADD(){

     setCalc (input1 + input2)

  }

  function SUB(){
  
     setCalc (input1 - input2)

  }

  function MUL(){

     setCalc (input1 * input2)
  }


  function DIV(){

    setCalc (input1 / input2)
  }

  function RESET(){
    setCalc('')
    setInput1('')
    setInput2('')
  }


  return (
    <>
    <Container>
    <Form>
        <Form.Group>
            <Form.Label>RESULT</Form.Label>
            <Form.Control 
                type="text" 
                value={calc} 
              
            />
        </Form.Group>

        <Form.Group>
            <Form.Label>Input 1</Form.Label>
            <Form.Control 
                type="number" 
                value={input1}
                onChange={e => setInput1(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group >
            <Form.Label>Input 2</Form.Label>
            <Form.Control 
              type="number" 
                value={input2} 
                onChange={e => setInput2(e.target.value)}
              required
            />
        </Form.Group>

            <Button variant="primary" onClick={ADD}>
               ADD
            </Button>
            <Button variant="primary" onClick={SUB}>
               SUBTRACT
            </Button>
            <Button variant="primary" onClick={MUL}>
                MULTIPLY
            </Button>
            <Button variant="primary" onClick={DIV}>
                DIVIDE
            </Button>
            <Button variant="primary" onClick={RESET}>
                RESET
            </Button>
            
    </Form>
</Container>
    
    </>
  );
}

export default App;
